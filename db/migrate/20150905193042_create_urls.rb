class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string :url
      t.string :uid
      t.references :user, index: true

      t.timestamps
    end
    add_index :urls, :uid, unique: true
    add_index :urls, :url
  end
end
