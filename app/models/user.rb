class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :recoverable, :trackable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :validatable

  validates :first_name, :presence => true, length: { maximum: 50 }, :format => { :with => /\A[a-zA-Z]+\z/, :message => "must contain only letters"}
  validates :last_name, :presence => true, length: { maximum: 50 }, :format => { :with => /\A[a-zA-Z]+\z/, :message => "must contain only letters"}
  has_many :urls, dependent: :destroy
end
