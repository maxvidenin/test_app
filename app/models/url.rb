class Url < ActiveRecord::Base
  belongs_to :user
  validates :url, presence: true
  validates :url, :format => URI::regexp(%w(http https))
  validates :user_id, presence: true
  validates :uid, uniqueness: true

  before_save :generate_url_uid

  def generate_url_uid
    begin
      self.uid = SecureRandom.hex(3).first(5)
    end while self.class.exists?(:uid => uid)
  end
end
