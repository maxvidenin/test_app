class SessionsController < Devise::SessionsController
  respond_to :js, only: [:new, :create]
end