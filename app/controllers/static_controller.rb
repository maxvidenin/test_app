class StaticController < ApplicationController
  def index
    @urls ||= current_user.urls.paginate(page: params[:page], :per_page => 5) if user_signed_in?
  end
end