class UrlsController < ApplicationController
  before_action :authenticate_user!

  def new
    @url = Url.new
  end

  def show
    url = Url.find_by!(uid: params[:uid])
    redirect_to url.url
  end

  def create
    @url = current_user.urls.find_by(url: url_params[:url])
    if @url.nil?
      @url = current_user.urls.build(url_params)
      @url.save
    end
    respond_to do |format|
      format.html { redirect_to root_path }
      format.js
    end
  end

  private

    def url_params
      params.require(:url).permit(:url,:user_id)
    end
end
