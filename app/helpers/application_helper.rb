module ApplicationHelper
  def user_name
    current_user.first_name + ' ' + current_user.last_name if user_signed_in?
  end
end
