FactoryGirl.define do
  factory :user do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    password '12312313'
    password_confirmation '12312313'
    email Faker::Internet.email
  end
end