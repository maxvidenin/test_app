require 'rails_helper'

describe User do

  before do
    @user = User.new(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name ,
      password: Faker::Internet.password(8, 20),
      email: Faker::Internet.email
    )
  end

  subject { @user }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_created_at) }
  it { should respond_to(:urls) }

  it { should be_valid }

  describe 'when first_name is not present' do
    before { @user.first_name = '' }
    it { should_not be_valid }
  end

  describe 'when password is not present' do
    before { @user.password = '' }
    it { should_not be_valid }
  end

  describe 'when email is not present' do
    before { @user.email = '' }
    it { should_not be_valid }
  end

  describe 'when invalid last_name' do
    before { @user.last_name = 'test123' }
    it { should_not be_valid }
  end

  describe 'when invalid password length' do
    before { @user.password = Faker::Internet.password(6)}
    it { should_not be_valid }
  end

  describe 'when first_name is too long' do
    before { @user.first_name = 'a' * 51 }
    it { should_not be_valid }
  end

  describe 'when last_name is too long' do
    before { @user.last_name = 'a' * 51 }
    it { should_not be_valid }
  end

  describe 'when email address is already taken' do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  describe 'when password does not match confirmation' do
    before { @user.password_confirmation = 'mismatch' }
    it { should_not be_valid }
  end
end