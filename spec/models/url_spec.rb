require 'rails_helper'

describe Url do

  before do
    user = User.create!(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name ,
      password: Faker::Internet.password(8, 20),
      email: Faker::Internet.email
    )

    @url = user.urls.build(
        url: Faker::Internet.url
    )
  end

  subject { @url }

  it { should respond_to(:url) }
  it { should respond_to(:user_id) }
  it { should respond_to(:uid) }
  it { should respond_to(:user) }


  it { should be_valid }

  describe 'when url is not present' do
    before { @url.url = '' }
    it { should_not be_valid }
  end

  describe 'when uid should be generated' do
    before { @url.save }
    it { should_not be_blank}
  end

  describe 'when user_id is not present' do
    before { @url.user_id = '' }
    it { should_not be_valid }
  end
end