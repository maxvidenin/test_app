require 'rails_helper'

describe "Static pages" do

  describe "Home page" do

    it "should have the content 'Numgames Test App'" do
      visit root_path
      expect(page).to have_content('Numgames Test App')
    end

    it "should have the title 'Home'" do
      visit root_path
      expect(page).to have_title('Numgames Test App | Home')
    end
  end
end
