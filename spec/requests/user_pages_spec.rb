require 'rails_helper'

describe 'User pages' do

  subject { page }

  describe 'signup page' do
    before { visit new_user_registration_path }

    it { should have_content('Sign up') }
    it { should have_title('Numgames Test App | Sign up') }

    let(:submit) { 'Sign up' }

    describe 'with invalid information' do
      it 'should not create a user' do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe 'after submission' do
      before { click_button submit }

      it { should have_title('Sign up') }
      it { should have_content('error') }
    end

    describe 'with valid information' do
      before do
        pass = Faker::Internet.password(8, 20)
        fill_in 'First name', with: Faker::Name.first_name
        fill_in 'Last name', with: Faker::Name.last_name
        fill_in 'Email', with: Faker::Internet.email
        fill_in 'Password', with: pass
        fill_in 'Password confirmation', with: pass
      end

      it 'should create a user' do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end
end
