require 'rails_helper'

describe 'Url pages' do

  subject { page }

  describe 'signin' do
    before { visit new_user_session_path }

    it { should have_title('Numgames Test App | Sign in') }

    describe 'creating a tiny url' do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in 'Email', with: user.email
        fill_in 'Password', with: user.password
        click_button 'Sign in'
      end

      it { should have_title('Numgames Test App | Home') }

      describe 'attempting to create a tiny url with valid information' do
        before do
          visit new_url_path
        end

        it { should have_content('Create a new tiny url') }

        describe 'with valid information' do
          before do
            fill_in 'url_url', with: Faker::Internet.url
          end

          it 'should create a tiny link' do
            expect { click_button 'Create' }.to change(Url, :count).by(1)
          end
        end

        describe 'with invalid information' do
          it 'should not create a tiny link' do
            expect { click_button 'Create' }.not_to change(Url, :count)
          end
        end
      end
    end
  end
end
