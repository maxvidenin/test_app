require 'rails_helper'

describe 'Authentication' do

  subject { page }

  describe 'signin' do
    before { visit new_user_session_path }

    describe 'with invalid information' do
      before { click_button 'Sign in' }

      it { should have_title('Numgames Test App | Sign in') }
      it { should have_content('Invalid') }

      describe 'after visiting another page' do
        before { click_link 'Home' }
        it { should_not have_selector('div.alert.alert-danger') }
      end
    end

    describe 'with valid information' do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in 'Email', with: user.email
        fill_in 'Password', with: user.password
        click_button 'Sign in'
      end

      it { should have_content(user.first_name) }
      it { should have_link('Create Tiny Url', href: new_url_path) }
      it { should have_link('Sign Out', href: destroy_user_session_path) }
      it { should_not have_link('Sign In', href: new_user_session_path) }

      describe 'followed by signout' do
        before { click_link 'Sign Out' }
        it { should have_link('Sign In') }
      end
    end
  end

  describe 'authorization' do

    describe 'for non-signed-in users' do
      let(:user) { FactoryGirl.create(:user) }

      describe 'when attempting to visit a protected page' do
        before do
          visit new_user_session_path
          fill_in 'Email', with: user.email
          fill_in 'Password', with: user.password
          click_button 'Sign in'
        end

        describe 'after signing in' do
          before { visit new_url_path }
          it 'should render the desired protected page' do
            expect(page).to have_content('Create a new tiny url')
          end
        end
      end

      describe 'in the Urls controller' do
        describe 'submitting to the create action' do
          before { post urls_path }
          specify { expect(response).to redirect_to(new_user_session_path) }
        end

        describe 'when attempting to visit a protected page' do
          before { visit new_url_path }
          it 'should redirect to sign in page' do
            expect(page).to have_content('Sign in')
          end
        end
      end
    end
  end
end
